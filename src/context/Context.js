import React from "react";
import data from "./data.json";

const { site: siteData, profile: profileData, data: earthquakeData } = data;

const ctx = React.createContext({
  siteData,
  profileData,
  earthquakeData
});

const useActive = () => {
  const [active, updateActive] = React.useState({
    id: 0,
    index: 0
  });

  const setActive = React.useCallback((activeObj) => {
    updateActive(activeObj);
  }, []);

  return {
    siteData,
    profileData,
    earthquakeData,
    active,
    setActive
  };
};

export default ctx;
export { useActive };
