import { Global, css } from "@emotion/react";
import colors from "./colors";

const GlobalStyles = () => (
  <Global
    styles={css`
      body {
        font-family: sans-serif;
        margin: 0;
        font-size: 16px;
        color: ${colors.darkGrey};
      }
    `}
  />
);

export default GlobalStyles;
