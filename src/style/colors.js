export default {
  lightGrey: "#ededed",
  mediumGrey: "#777777",
  darkGrey: "#444444",
  blue: "#0011bb",
  purple: "#6600cc"
};
