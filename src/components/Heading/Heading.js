import styled from "@emotion/styled";
import colors from "../../style/colors";

const Heading = (props) => <StyledHeading data-testid="heading" {...props} />;

const StyledHeading = styled.h1`
  font-size: 18px;
  color: ${colors.darkGrey};
  text-align: ${(props) => props.align || "left"};
`;

export default Heading;
