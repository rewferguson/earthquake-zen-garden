import Heading from "./Heading";
import renderer from "react-test-renderer";
import { render, screen } from "@testing-library/react";

it("renders", () => {
  const tree = renderer.create(<Heading>Test Heading</Heading>).toJSON();
  expect(tree).toMatchSnapshot();
});

it("displays align prop", () => {
  render(<Heading align="center">Test Heading</Heading>);

  expect(screen.getByTestId("heading")).toHaveStyle({ textAlign: "center" });
});
