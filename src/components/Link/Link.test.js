import Link from "./Link";
import { render, screen } from "@testing-library/react";

it("will render", () => {
  render(<Link to="/">Test Link</Link>);

  expect(screen.getByTestId("link")).toBeInTheDocument;
});
