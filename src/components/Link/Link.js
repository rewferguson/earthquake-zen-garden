import { Link as RouterLink } from "@reach/router";
import styled from "@emotion/styled";

import colors from "../../style/colors";

const Link = (props) => <StyledLink data-testid="link" {...props} />;

const StyledLink = styled(RouterLink)`
  color: ${colors.blue};

  &:visited {
    color: ${colors.purple};
  }
`;

export default Link;
