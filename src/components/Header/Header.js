import { useContext } from "react";
// import { Link } from "@reach/router";
import Link from "../Link";
import appData from "../../Context";
import styled from "@emotion/styled";
import colors from "../../style/colors";

const Container = ({ content }) => {
  const { siteData, profileData } = useContext(appData);
  return (
    <Header data-testid="header">
      <Nav>
        <NavList>
          <Logo>
            <Link to="/">
              <img src={siteData.logoImage} alt="Realtor.com" />
            </Link>
          </Logo>
          <AppTitle>
            <Link to="/">
              <h1>{siteData.title}</h1>
            </Link>
          </AppTitle>
          <ProfileLink>
            <Link to="/profile">Welcome {profileData.firstName}</Link>
          </ProfileLink>
        </NavList>
      </Nav>
    </Header>
  );
};

const Header = styled.header`
  background: #ededed;
  padding: 20px;
`;

const Nav = styled.nav``;

const NavList = styled.ul`
  margin: 0;
  padding: 0;
  display: flex;
  list-style: none;
  justify-content: space-between;
  align-items: center;
`;

const Logo = styled.li`
  width: 35px;

  img {
    width: 100%;
  }
`;

const AppTitle = styled.li`
  a {
    text-decoration: none;
  }
  h1 {
    margin: 0;
    color: ${colors.mediumGrey};
  }
`;

const ProfileLink = styled.li`
  font-weight: bold;
`;

export default Container;
