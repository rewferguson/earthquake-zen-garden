import Header from "./Header";
import renderer from "react-test-renderer";

it("renders", () => {
  const tree = renderer.create(<Header />).toJSON();
  expect(tree).toMatchSnapshot();
});
