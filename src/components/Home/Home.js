import React, { useEffect, useContext, useMemo, useState } from "react";
import { useTable, useSortBy } from "react-table";
import styled from "@emotion/styled";

import Link from "../Link";
import appData from "../../Context";
import colors from "../../style/colors";

import Date from "../Date";
import Heading from "../Heading";

const Home = (props) => {
  const { earthquakeData } = useContext(appData);

  const tableColumns = useMemo(
    () => [
      {
        Header: "Title",
        accessor: "title"
      },
      {
        Header: "Magnitude",
        accessor: "magnitude"
      },
      {
        Header: "Time",
        accessor: "time"
      },
      {
        Header: "ID",
        accessor: "id",
        isVisible: false
      }
    ],
    []
  );

  const tableData = useMemo(() => {
    return earthquakeData.features.map((feature) => {
      return {
        title: feature.properties.title,
        magnitude: feature.properties.mag,
        time: feature.properties.time,
        id: feature.id
      };
    });
  }, [earthquakeData]);

  const tableInstance = useTable(
    { columns: tableColumns, data: tableData },
    useSortBy
  );

  const {
    allColumns,
    setHiddenColumns,
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow
  } = tableInstance;

  useMemo(() => setHiddenColumns(["id"]), []);

  return (
    <section data-testid="home">
      <Heading align="center">USGS All Earthquakes, Past Hour</Heading>

      <table {...getTableProps()} style={{ width: "100%" }}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => {
                return (
                  <TableHeading
                    {...column.getHeaderProps(column.getSortByToggleProps())}
                  >
                    {column.render("Header")}
                  </TableHeading>
                );
              })}
            </tr>
          ))}
        </thead>
        <TableBody {...getTableBodyProps()}>
          {rows.map((row) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  switch (cell.column.Header) {
                    case "Title":
                      return (
                        <td {...cell.getCellProps()}>
                          <Link to={`/details/${cell.row.values.id}`}>
                            {cell.value}
                          </Link>
                        </td>
                      );
                    case "Magnitude":
                      return (
                        <MagnitudeCell {...cell.getCellProps()}>
                          {cell.render("Cell")}
                        </MagnitudeCell>
                      );
                    case "Time":
                      return (
                        <td {...cell.getCellProps()}>
                          <Date value={cell.value} />
                        </td>
                      );
                    default:
                      return (
                        <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                      );
                  }
                })}
              </tr>
            );
          })}
        </TableBody>
      </table>

      {/*
      <table>
        <thead>
          <tr>
            <TableHeading>Title</TableHeading>
            <TableHeading>Magnitude</TableHeading>
            <TableHeading>Time</TableHeading>
          </tr>
        </thead>
        <TableBody>
          {earthquakeData.features.map((feature) => (
            <tr key={feature.id}>
              <td>
                <Link to={`/details/${feature.id}`}>
                  {feature.properties.place}
                </Link>
              </td>
              <MagnitudeCell>{feature.properties.mag}</MagnitudeCell>
              <td>
                <Date value={feature.properties.time} />
              </td>
            </tr>
          ))}
        </TableBody>
      </table>
      */}
    </section>
  );
};

const TableHeading = styled.th`
  font-weight: normal;
  text-align: center;
`;

const TableBody = styled.tbody`
  font-weight: bold;
`;

const MagnitudeCell = styled.td`
  text-align: center;
  padding: 0 20px;
`;

export default Home;
