import Home from "./Home";
import { render, screen } from "@testing-library/react";
import renderer from "react-test-renderer";

it("renders", () => {
  const tree = renderer.create(<Home />).toJSON();
  expect(tree).toMatchSnapshot();
});

test("component will render", () => {
  render(<Home />);

  expect(screen.getByTestId("home")).toBeInTheDocument;
});
