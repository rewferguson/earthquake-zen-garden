import { useContext } from "react";
import styled from "@emotion/styled";

import appData from "../../context";
import Heading from "../Heading";
import TextRow from "../TextRow";

const Profile = () => {
  const { profileData } = useContext(appData);
  return (
    <section data-testid="profile">
      <Heading align="center">Profile</Heading>
      <Container>
        <div>
          <img
            src={profileData.avatarImage}
            alt={`${profileData.firstName} ${profileData.lastName}`}
            style={{ maxWidth: "150px", marginRight: "20px" }}
          />
        </div>

        <div>
          <TextRow label="First name" value={profileData.firstName} />
          <TextRow label="Last name" value={profileData.lastName} />
          <TextRow label="Phone" value={profileData.phone} />
          <TextRow label="Email" value={profileData.email} />
          <TextRow label="Bio" value={profileData.bio} />
        </div>
      </Container>
    </section>
  );
};

const Container = styled.div`
  display: flex;
`;

export default Profile;
