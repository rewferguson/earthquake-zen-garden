import Profile from "./Profile";
import renderer from "react-test-renderer";

it("renders", () => {
  const tree = renderer.create(<Profile />).toJSON();
  expect(tree).toMatchSnapshot();
});
