import React, { useContext, useEffect } from "react";
import { Router, Link } from "@reach/router";
import AppData, { useActive } from "../../Context";

import Header from "../Header";
import Home from "../Home";
import Profile from "../Profile";
import Details from "../Details";

import Global from "../../style/Global";
import styled from "@emotion/styled";

const App = () => {
  const activeHook = useActive();

  return (
    <React.Fragment>
      <Global />
      <AppData.Provider value={activeHook}>
        <Header />
        <Main>
          <Router>
            <Profile path="profile" />
            <Details path="/details/:id" />
            <Home path="/" default />
          </Router>
        </Main>
      </AppData.Provider>
    </React.Fragment>
  );
};

const Main = styled.main`
  max-width: 575px;
  margin: 0 auto;
`;

export default App;
