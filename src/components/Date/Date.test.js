import Date from "./Date";
import { render, screen } from "@testing-library/react";

it("renders correct string", () => {
  const { rerender } = render(<Date value={1523646000170} />);
  expect(screen.getByTestId("formattedDate")).toHaveTextContent(
    "April 13, 2018, 2:00 PM"
  );

  rerender(<Date value={1523645022880} />);
  expect(screen.getByTestId("formattedDate")).toHaveTextContent(
    "April 13, 2018, 1:43 PM"
  );
});
