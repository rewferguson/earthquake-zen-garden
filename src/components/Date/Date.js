const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];

const formatTime = (dateObj) => {
  const hours = dateObj.getHours();
  const minutes = dateObj.getMinutes();
  const afternoon = hours > 12 ? true : false;

  return `${afternoon ? hours - 12 : hours}:${
    minutes > 9 ? minutes : "0" + minutes.toString()
  } ${afternoon ? "PM" : "AM"}`;
};

const FormattedDate = ({ value }) => {
  const dateObj = new Date(value);
  return (
    <span data-testid="formattedDate">
      {months[dateObj.getMonth()]} {dateObj.getDate()}, {dateObj.getFullYear()},{" "}
      {formatTime(dateObj)}
    </span>
  );
};

export default FormattedDate;
