import styled from "@emotion/styled";
import colors from "../../style/colors";

const TextRow = ({ label, value }) => (
  <Row data-testid="textrow">
    <Label>{label}</Label>
    <Value>{value}</Value>
  </Row>
);

const Row = styled.div`
  display: flex;
`;

const Label = styled.div`
  width: 120px;
  font-weight: bold;
  flex-shrink: 0;
`;

const Value = styled.div`
  color: ${colors.mediumGrey};
  font-weight: bold;
`;

export default TextRow;
