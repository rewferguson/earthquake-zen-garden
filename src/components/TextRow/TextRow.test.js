import TextRow from "./TextRow";
import { render, screen } from "@testing-library/react";

it("will render", () => {
  render(<TextRow label="Hello" value="World" />);

  const element = screen.getByTestId("textrow");
  expect(element).toBeInTheDocument;
  expect(element).toHaveTextContent("Hello");
  expect(element).toHaveTextContent("World");
});
