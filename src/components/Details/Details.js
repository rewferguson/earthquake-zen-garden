import React, { useContext, useEffect, useState } from "react";
import { css } from "@emotion/react";

import appData from "../../Context";
import Heading from "../Heading";
import TextRow from "../TextRow";
import Date from "../Date";

const sectionStyles = css`
  padding: 0 50px;
`;

const Details = (props) => {
  const { earthquakeData, active, setActive } = useContext(appData);
  const [selection, setSelection] = useState({});

  useEffect(() => {
    if (!active || !props.id) {
      return;
    }

    if (active.id !== props.id) {
      const activeIndex = earthquakeData.features.findIndex(
        (feature) => feature.id === props.id
      );

      setActive({
        index: activeIndex,
        id: props.id
      });
    }

    setSelection(earthquakeData.features[active.index]);
  }, [active, props.id]);

  return selection.properties ? (
    <section data-testid="details" css={sectionStyles}>
      <Heading align="left">{selection.properties.title}</Heading>

      <TextRow label="Title" value={selection.properties.title} />
      <TextRow label="Magnitude" value={selection.properties.mag} />
      <TextRow
        label="Time"
        value={<Date value={selection.properties.time} />}
      />
      <TextRow label="Status" value={selection.properties.status} />
      <TextRow label="Tsunami" value={selection.properties.tsunami} />
      <TextRow label="Type" value={selection.properties.type} />
    </section>
  ) : (
    <div data-testid="loading">Loading...</div>
  );
};

export default Details;
