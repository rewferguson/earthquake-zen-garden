import React from "react";
import Details from "./Details";
import AppData, { useActive } from "../../Context";
import { render, screen } from "@testing-library/react";
import renderer from "react-test-renderer";
import data from "../../context/data.json";

const { data: earthquakeData } = data;

const active = {
  active: {
    // id: "nc72999021",
    id: "",
    index: 0
  },
  setActive: () => 0,
  earthquakeData
};

const Component = ({ data, id = "" }) => (
  <AppData.Provider value={data}>
    <Details id={id} />
  </AppData.Provider>
);

it("renders loading with no data", () => {
  render(
    <Component
      data={{
        active: {
          id: "",
          index: 0
        },
        setActive: () => 0,
        earthquakeData
      }}
      id=""
    />
  );
  expect(screen.getByTestId("loading")).toBeInTheDocument;
});

it("renders details when data exists", () => {
  render(
    <Component
      data={{
        active: {
          id: "nc72999021",
          index: 0
        },
        setActive: () => 0,
        earthquakeData
      }}
      id="nc72999021"
    />
  );

  expect(screen.getByTestId("details")).toBeInTheDocument;
});
